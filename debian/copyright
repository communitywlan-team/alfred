Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: A.L.F.R.E.D.
Upstream-Contact: b.a.t.m.a.n@lists.open-mesh.org
Source: https://git.open-mesh.org/alfred.git

Files: *
Copyright: 2009-2020, Andrew Lunn <andrew@lunn.ch>
           2006-2020, Marek Lindner <marek.lindner@mailbox.org>
           2006-2020, Simon Wunderlich <sw@simonwunderlich.de>
           2006-2020, Sven Eckelmann <sven@narfation.org>
License: GPL-2

Files: batman_adv.h
Copyright: 2016-2020, Matthias Schiffer <mschiffer@universe-factory.net>
License: MIT

Files: bitops.h
Copyright: 2012-2020, Sven Eckelmann <sven@narfation.org>
License: MIT

Files: list.h
Copyright: 1991-2013 linux kernel contributors
License: GPL-2

Files: debian/*
Copyright: 2014-2015 Steffen Moeller <moeller@debian.org>
           2015      Franz Schrober <franzschrober@yahoo.de>
           2020-2022 Sven Eckelmann <sven@narfation.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
